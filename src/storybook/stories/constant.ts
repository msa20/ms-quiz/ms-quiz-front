import { IdName } from '~/comp/shared';


export const quizAnswerSheetId = '69636a55-65cd-41a6-bede-8b2e3b8d2276';
export const quizSheetId = 'c456dfa6-e416-4310-b47d-5c5fbaf7241c';
export const groupId = 'testGroup-1';
export const writer = new IdName('testWriter-1', '홍길동');
export const writer2 = new IdName('testWriter-2', '김길동');
export const writerId = writer.id;
