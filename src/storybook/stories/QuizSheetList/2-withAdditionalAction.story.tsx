import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizSheetList, QuizSheetForm } from '@nextree-ms/quiz';
import { Button, DialogTitle, Dialog, DialogContent } from '@material-ui/core';
import { groupId, writer } from '../constant';


interface Props {

}

interface State {
  open: boolean;
  listInitTrigger: boolean;
}

export const withAdditionalAction = withStory(
  //
  class Story extends StoryComponent<Props, State> {
    //
    state = { open: false, listInitTrigger: false };

    handleClose() {
      this.hide();
    }

    onClick() {
      this.show();
    }

    show() {
      this.setState({ open: true });
    }

    hide() {
      this.setState({ open: false });
    }

    onSuccessRegister() {
      //
      this.setState({ listInitTrigger: !this.state.listInitTrigger });
      this.hide();
    }

    render() {
      const { open, listInitTrigger } = this.state;
      return (
        <QuizSheetList
          groupId={groupId}
          limit={10}
          initTrigger={listInitTrigger}
          additionalAction={
            <>
              <Button variant="contained" onClick={this.onClick}>시험지 작성하기</Button>
              <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle id="customized-dialog-title">
                  시험지 작성
                </DialogTitle>
                <DialogContent dividers>
                  <QuizSheetForm
                    groupId={groupId}
                    writer={writer}
                    onSuccess={() => this.onSuccessRegister()}
                  />
                </DialogContent>
              </Dialog>
            </>
            }
        />
      );
    }
  }
);

withAdditionalAction.storyName = 'withAdditionalAction';
withAdditionalAction.parameters = {};
