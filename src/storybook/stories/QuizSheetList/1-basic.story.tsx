import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizSheetList } from '@nextree-ms/quiz';


export const basic = withStory(

  class Story extends StoryComponent {
    private groupId = 'testGroup-1';

    render() {
      return (
        <QuizSheetList groupId={this.groupId} />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
