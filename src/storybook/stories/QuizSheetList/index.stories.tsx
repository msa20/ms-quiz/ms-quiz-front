import path from 'paths.macro';
import { docsUtils } from '@nara.platform/storybook';
import { QuizSheetList } from '@nextree-ms/quiz';


export * from './1-basic.story';
export * from './2-withAdditionalAction.story';

export default docsUtils.componentDocs({
  path,
  component: QuizSheetList,
});
