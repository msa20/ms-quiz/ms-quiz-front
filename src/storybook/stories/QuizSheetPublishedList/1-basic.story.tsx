import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizSheetPublishedList } from '~/comp';


export const basic = withStory(

  class Story extends StoryComponent {
    private groupId = 'testGroup-1';

    onClick(event: React.MouseEvent, quizSheetId: string) {
      //
      console.log('quizSheetId: ' + quizSheetId);
    }


    render() {
      return (
        <QuizSheetPublishedList
          groupId={this.groupId}
          onClickButton={(event: React.MouseEvent, quizSheetId: string) => this.onClick(event, quizSheetId)}
          // quizSheetId={this.quizSheetId}
          // writer={writer}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
