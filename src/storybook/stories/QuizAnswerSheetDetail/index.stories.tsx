import path from 'paths.macro';
import { docsUtils } from '@nara.platform/storybook';
import { QuizAnswerSheetDetail } from '~/comp';


export * from './1-basic.story';

export default docsUtils.componentDocs({
  path,
  component: QuizAnswerSheetDetail,
});
