import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { quizAnswerSheetId, groupId, writer } from '../constant';
import { QuizAnswerSheetDetail } from '~/comp';


export const basic = withStory(

  class Story extends StoryComponent {
    //


    render() {
      return (
        <QuizAnswerSheetDetail
          groupId={groupId}
          quizAnswerSheetId={quizAnswerSheetId}
          writer={writer}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
