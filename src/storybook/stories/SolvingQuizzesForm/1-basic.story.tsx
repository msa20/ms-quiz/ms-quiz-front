import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { SolvingQuizzesForm } from '@nextree-ms/quiz';
import { quizSheetId, groupId, writer2 } from '../constant';


export const basic = withStory(

  class Story extends StoryComponent {
    // DB에서 quizSheetId 확인하여 입력

    render() {
      return (
        <SolvingQuizzesForm
          writer={writer2}
          groupId={groupId}
          quizSheetId={quizSheetId}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
