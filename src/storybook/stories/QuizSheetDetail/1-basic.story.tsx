import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizSheetDetail } from '~/comp';
import { writer, quizSheetId } from '../constant';


export const basic = withStory(

  class Story extends StoryComponent {
    // QuizSheetForm 에서 시험지 등록 후 DB에서 아이디 확인하여 입력

    render() {
      return (
        <QuizSheetDetail
          writer={writer}
          quizSheetId={quizSheetId}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
