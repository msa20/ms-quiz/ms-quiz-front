import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { Button } from '@material-ui/core';
import { groupId, writer } from '../constant';


export const basic = withStory(

  class Story extends StoryComponent {
    // QuizSheetForm 에서 시험지 등록 후 DB에서 quizSheetId 확인하여 입력
    private quizSheetId = '973ca713-87f8-44b5-85c9-59033f5cbd3b';

    render() {
      return (
        <Button />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
