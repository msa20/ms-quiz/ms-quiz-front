import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizAnswerSheetList } from '@nextree-ms/quiz';
import { writer } from '~/storybook/stories/constant';


export const basic = withStory(

  class Story extends StoryComponent {
    private groupId = 'testGroup-1';
    private quizSheetId = 'quizsheet1';

    render() {
      return (
        <QuizAnswerSheetList
          groupId={this.groupId}
          quizSheetId={this.quizSheetId}
          writer={writer}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
