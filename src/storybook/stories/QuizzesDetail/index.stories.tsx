import path from 'paths.macro';
import { docsUtils } from '@nara.platform/storybook';
import { QuizzesDetail } from '@nextree-ms/quiz';


export * from './1-basic.story';

export default docsUtils.componentDocs({
  path,
  component: QuizzesDetail,
});
