import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizzesDetail } from '~/comp';
import { groupId, writer } from '../constant';


export const basic = withStory(

  class Story extends StoryComponent {
    // QuizSheetForm 에서 시험지 등록 후 DB에서 아이디 확인하여 입력
    private quizSheetId = '9fecef35-7867-4f15-b9c7-556f2543eecb';

    render() {
      return (
        <QuizzesDetail
          groupId={groupId}
          writer={writer}
          quizSheetId={this.quizSheetId}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
