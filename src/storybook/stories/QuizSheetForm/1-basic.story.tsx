import React from 'react';
import { StoryComponent, withStory } from '@nara.platform/storybook';
import { QuizSheetForm } from '@nextree-ms/quiz';
import { groupId, writer } from '../constant';


export const basic = withStory(

  class Story extends StoryComponent {
    //
    render() {
      return (
        <QuizSheetForm
          groupId={groupId}
          writer={writer}
        />
      );
    }
  }
);

basic.storyName = 'basic';
basic.parameters = {};
