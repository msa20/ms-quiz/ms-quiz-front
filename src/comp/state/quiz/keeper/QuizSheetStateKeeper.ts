import { makeAutoObservable, runInAction } from 'mobx';
import { CommandResponse, NameValueList, IdName } from '~/comp/shared';

import { QuizSheet, QuizSheetApiStub, QuizSheetCdo, QuizSheetQueryApiStub, QuizSheetQuery } from '~/comp/api/quiz';
import { QuizSheetFlowApiStub } from '~/comp/api/quiz-flow';


class QuizSheetStateKeeper {
  static readonly instanceName: string = 'quizSheetStateKeeper';
  static instance: QuizSheetStateKeeper;

  private readonly quizSheetApi: QuizSheetApiStub;
  private readonly quizSheetQueryApi: QuizSheetQueryApiStub;
  private readonly quizSheetFlowApi: QuizSheetFlowApiStub;

  quizSheet: QuizSheet | null = null;

  constructor(
    quizSheetApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizSheetQueryApi: QuizSheetQueryApiStub = QuizSheetQueryApiStub.instance,
    quizSheetFlowApi: QuizSheetFlowApiStub = QuizSheetFlowApiStub.instance,
  ) {
    this.quizSheetApi = quizSheetApi;
    this.quizSheetQueryApi = quizSheetQueryApi;
    this.quizSheetFlowApi = quizSheetFlowApi;
    makeAutoObservable(this);
  }

  init(groupId: string) {
    this.quizSheet = QuizSheet.new(groupId);
  }

  initSheet(groupId: string, writer: IdName) {
    this.quizSheet = QuizSheet.newDefault(groupId, writer);
  }

  setQuizSheetProp(name: keyof QuizSheet, value: any) {
    if (!this.quizSheet) {
      throw new Error('QuizSheetStateKeeper.setQuizSheetProp -> quizSheet is null');
    }
    (this.quizSheet as any)[name] = value;
    console.log('this.quizSheet', this.quizSheet);
  }

  clear() {
    this.quizSheet = null;
  }

  async save(quizSheet: QuizSheet): Promise<CommandResponse> {
    //
    const isNew = !quizSheet.id;
    let response;

    if (isNew) {
      response = await this.register(QuizSheetCdo.fromModel(quizSheet));
    }
    else {
      response = await this.modify(quizSheet.id, QuizSheet.asNameValues(quizSheet));
    }

    return response;
  }

  async register(quizSheetCdo: QuizSheetCdo): Promise<CommandResponse> {
    // return this.quizSheetApi.registerQuizSheet(quizSheetCdo);

    const response = await this.quizSheetApi.registerQuizSheet(quizSheetCdo);

    console.log(response);
    return response;
  }

  async modify(quizSheetId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.quizSheetApi.modifyQuizSheet(quizSheetId, nameValues);
  }

  async remove(quizSheetId: string): Promise<CommandResponse> {
    return this.quizSheetApi.removeQuizSheet(quizSheetId);
  }

  async findQuizSheetByGroupId(groupId: string): Promise<QuizSheet> {
    const quizSheetQuery = QuizSheetQuery.by(groupId);
    const quizSheet = await this.quizSheetQueryApi.executeQuizSheetQuery(quizSheetQuery);

    runInAction(() => this.quizSheet = quizSheet);
    return quizSheet;
  }

  async findQuizSheetById(quizSheetId: string): Promise<QuizSheet> {
    const quizSheetQuery = QuizSheetQuery.by(quizSheetId);
    const quizSheet = await this.quizSheetQueryApi.executeQuizSheetQuery(quizSheetQuery);

    runInAction(() =>
      this.quizSheet = quizSheet
    );
    return quizSheet;
  }

  async takeQuiz(quizSheetId: string, writer: IdName): Promise<CommandResponse> {
    return this.quizSheetFlowApi.takeQuiz(quizSheetId, writer);
  }
}

QuizSheetStateKeeper.instance = new QuizSheetStateKeeper();

export default QuizSheetStateKeeper;

