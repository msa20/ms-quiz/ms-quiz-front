import { makeAutoObservable, runInAction } from 'mobx';

import { CommandResponse, NameValueList, Offset, QueryParam, Operator } from '~/comp/shared';
import { Quiz, QuizCdo, QuizItem, QuizQueryApiStub, QuizSheetApiStub, QuizzesDynamicQuery } from '~/comp/api/quiz';


class QuizzesStateKeeper {
  static readonly instanceName: string = 'quizzesStateKeeper';
  static instance: QuizzesStateKeeper;

  private readonly quizApi: QuizSheetApiStub;
  private readonly quizQueryApi: QuizQueryApiStub;

  currentIndex: number = 0;

  quizzes: Quiz[] = [];

  constructor(
    quizApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizQueryApi: QuizQueryApiStub = QuizQueryApiStub.instance

  ) {
    this.quizApi = quizApi;
    this.quizQueryApi = quizQueryApi;
    makeAutoObservable(this);
  }

  setQuizProp(index: number, name: keyof Quiz, value: any) {
    if (!this.quizzes || !this.quizzes[index]) {
      throw new Error(`QuizzesStateKeeper.setQuizProp -> quizzes[${index}] is null`);
    }
    (this.quizzes[index] as any)[name] = value;

    console.log('this.quizzes', this.quizzes);
  }

  setQuizItemProp(index: number, quizItemIndex: number, name: keyof QuizItem, value: any) {
    if (!this.quizzes[index] || !this.quizzes[index].quizItems) {
      throw new Error(`QuizzesStateKeeper.setQuizProp -> quizzes[${index}] is null`);
    }
    (this.quizzes[index].quizItems[quizItemIndex] as any)[name] = value;
  }

  clear() {
    this.quizzes = [];
  }

  addQuiz(quiz: Quiz) {
    console.log('quiz', quiz);
    this.quizzes.push(quiz);
  }

  setQuizzes(quizzes: Quiz[]) {
    this.quizzes = quizzes;
  }

  addQuizItem(quizItem: QuizItem, index: number) {
    //
    this.quizzes[index].quizItems.push(quizItem);
  }

  deleteQuiz(index: number) {
    //
    this.quizzes.splice(index, 1);
  }

  substractQuizItem(quizIndex: number, quizItemIndex: number) {
    //
    const targetQuiz = this.quizzes[quizIndex];

    const newQuizItems = targetQuiz.quizItems.filter((quizItem, index) => quizItemIndex !== index);

    targetQuiz.quizItems = newQuizItems;
  }

  async save(quizzes: Quiz[]): Promise<CommandResponse> {
    //
    const response = await this.register(QuizCdo.fromModels(quizzes));

    return response;
  }

  async register(quizCdos: QuizCdo[]): Promise<CommandResponse> {
    return this.quizApi.registerQuizs(quizCdos);
  }

  async modify(quizId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.quizApi.modifyQuiz(quizId, nameValues);
  }

  async findQuizzesByQuizSheetId(quizSheetId: string, offset: Offset): Promise<Quiz[]> {
    //
    const query = QuizzesDynamicQuery.oneParam<Quiz[]>(
      QueryParam.endParam('quizSheetId', Operator.Equal, quizSheetId),
    );

    query.offset = offset;

    const quizzes = await this.quizQueryApi.executeQuizzesDynamicQuery(query);

    runInAction(() => {
      this.quizzes = quizzes;
    });

    return quizzes;
  }

}

QuizzesStateKeeper.instance = new QuizzesStateKeeper();

export default QuizzesStateKeeper;
