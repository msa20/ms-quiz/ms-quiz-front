import { makeAutoObservable, runInAction } from 'mobx';
import { CommandResponse, IdName, NameValueList, QueryParam } from '~/comp/shared';

import {
  QuizAnswerSheet,
  QuizAnswerSheetCdo,
  QuizAnswerSheetDynamicQuery,
  QuizAnswerSheetQuery,
  QuizAnswerSheetQueryApiStub,
  QuizSheetApiStub,
} from '~/comp/api/quiz';
import { Operator } from '~/comp';


class QuizAnswerSheetStateKeeper {
  static readonly instanceName: string = 'quizAnswerSheetStateKeeper';
  static instance: QuizAnswerSheetStateKeeper;

  private readonly quizAnswerSheetApi: QuizSheetApiStub;
  private readonly quizAnswerSheetQueryApi: QuizAnswerSheetQueryApiStub;

  quizAnswerSheet: QuizAnswerSheet | null = null;

  constructor(
    quizAnswerSheetApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizAnswerSheetQueryApi: QuizAnswerSheetQueryApiStub = QuizAnswerSheetQueryApiStub.instance
  ) {
    this.quizAnswerSheetApi = quizAnswerSheetApi;
    this.quizAnswerSheetQueryApi = quizAnswerSheetQueryApi;
    makeAutoObservable(this);
  }

  init(groupId: string, quizSheetId: string) {
    this.quizAnswerSheet = QuizAnswerSheet.new(groupId, quizSheetId);
  }

  setQuizAnswerSheetProp(name: keyof QuizAnswerSheet, value: any) {
    if (!this.quizAnswerSheet) {
      throw new Error('QuizAnswerSheetStateKeeper.setQuizAnswerSheetProp -> quizAnswerSheet is null');
    }
    (this.quizAnswerSheet as any)[name] = value;
  }

  clear() {
    this.quizAnswerSheet = null;
  }

  async register(quizAnswerSheetCdo: QuizAnswerSheetCdo): Promise<CommandResponse> {
    return this.quizAnswerSheetApi.registerQuizAnswerSheet(quizAnswerSheetCdo);
  }

  async modify(quizAnswerSheetId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.quizAnswerSheetApi.modifyQuizAnswerSheet(quizAnswerSheetId, nameValues);
  }

  async remove(quizAnswerSheetId: string): Promise<CommandResponse> {
    return this.quizAnswerSheetApi.removeQuizAnswerSheet(quizAnswerSheetId);
  }

  async findQuizAnswerSheetById(quizAnswerSheetId: string): Promise<QuizAnswerSheet> {
    const quizAnswerSheetQuery = QuizAnswerSheetQuery.by(quizAnswerSheetId);
    const quizAnswerSheet = await this.quizAnswerSheetQueryApi.executeQuizAnswerSheetQuery(quizAnswerSheetQuery);

    runInAction(() =>
      this.quizAnswerSheet = quizAnswerSheet
    );
    return quizAnswerSheet;
  }

  async findQuizAnswerSheetByQuizSheetId(quizSheetId: string, writerId: string): Promise<QuizAnswerSheet | null> {
    const query = QuizAnswerSheetDynamicQuery.multiParams<QuizAnswerSheet>(
      QueryParam.andParam('quizSheetId', Operator.Equal, quizSheetId),
      QueryParam.endParam('writerId', Operator.Equal, writerId),
    );

    const quizAnswerSheet = await this.quizAnswerSheetQueryApi.executeQuizAnswerSheetDynamicQuery(query)
      .catch(() => null);

    runInAction(() => {
      this.quizAnswerSheet = quizAnswerSheet;
    });

    return quizAnswerSheet;
  }

}

QuizAnswerSheetStateKeeper.instance = new QuizAnswerSheetStateKeeper();

export default QuizAnswerSheetStateKeeper;
