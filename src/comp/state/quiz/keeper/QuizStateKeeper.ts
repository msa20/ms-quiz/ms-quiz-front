import { makeAutoObservable, runInAction } from 'mobx';
import { CommandResponse, NameValueList, IdName } from '~/comp/shared';

import { Quiz, QuizCdo, QuizQueryApiStub, QuizSheetApiStub, QuizQuery } from '~/comp/api/quiz';


class QuizStateKeeper {
  static readonly instanceName: string = 'quizStateKeeper';
  static instance: QuizStateKeeper;

  private readonly quizApi: QuizSheetApiStub;
  private readonly quizQueryApi: QuizQueryApiStub;

  quiz: Quiz | null = null;

  constructor(
    quizApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizQueryApi: QuizQueryApiStub = QuizQueryApiStub.instance
  ) {
    this.quizApi = quizApi;
    this.quizQueryApi = quizQueryApi;
    makeAutoObservable(this);
  }

  init(groupId: string, quizSheetId: string) {
    this.quiz = Quiz.new(groupId, quizSheetId);
  }

  initQuizForm(groupId: string, quizSheetId: string, writer: IdName) {
    this.quiz = Quiz.newDefault(groupId, quizSheetId, writer);

    return this.quiz;
  }

  setQuizProp(name: keyof Quiz, value: any) {
    if (!this.quiz) {
      throw new Error('QuizStateKeeper.setQuizProp -> quiz is null');
    }
    (this.quiz as any)[name] = value;
  }

  setQuiz(quiz: Quiz) {
    this.quiz = quiz;
  }

  clear() {
    this.quiz = null;
  }

  async save(quiz: Quiz): Promise<CommandResponse> {
    //
    console.log('save domain', quiz);
    const isNew = !quiz.id;
    let response;

    if (isNew) {
      response = await this.register(QuizCdo.fromModel(quiz));
    }
    else {
      response = await this.modify(quiz.id, Quiz.asNameValues(quiz));
    }

    return response;
  }

  async register(quizCdo: QuizCdo): Promise<CommandResponse> {
    return this.quizApi.registerQuiz(quizCdo);
  }

  async modify(quizId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.quizApi.modifyQuiz(quizId, nameValues);
  }

  async remove(quizId: string): Promise<CommandResponse> {
    return this.quizApi.removeQuiz(quizId);
  }

  async findQuizById(quizId: string): Promise<Quiz> {
    const quizQuery = QuizQuery.by(quizId);
    const quiz = await this.quizQueryApi.executeQuizQuery(quizQuery);

    runInAction(() =>
      this.quiz = quiz
    );
    return quiz;
  }

}

QuizStateKeeper.instance = new QuizStateKeeper();

export default QuizStateKeeper;
