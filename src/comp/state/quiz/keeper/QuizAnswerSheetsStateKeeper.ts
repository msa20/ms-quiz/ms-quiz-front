import { makeAutoObservable, runInAction } from 'mobx';
import { Offset, Operator, QueryParam } from '~/comp/shared';
import {
  QuizAnswerSheet,
  QuizAnswerSheetQueryApiStub,
  QuizAnswerSheetsDynamicQuery,
} from '~/comp/api';


class QuizAnswerSheetsStateKeeper {
  static readonly instanceName: string = 'quizAnswerSheetsStateKeeper';
  static instance: QuizAnswerSheetsStateKeeper;

  private readonly quizAnswerSheetQueryApi: QuizAnswerSheetQueryApiStub;

  quizAnswerSheets: QuizAnswerSheet[] = [];

  constructor(
    quizAnswerSheetQueryApi: QuizAnswerSheetQueryApiStub = QuizAnswerSheetQueryApiStub.instance
  ) {
    this.quizAnswerSheetQueryApi = quizAnswerSheetQueryApi;
    makeAutoObservable(this);
  }

  setQuizAnswerSheetProp(index: number, name: keyof QuizAnswerSheet, value: any) {
    if (!this.quizAnswerSheets || this.quizAnswerSheets[index]) {
      throw new Error(`QuizAnswerSheetsStateKeeper.setQuizAnswerSheetProp -> quizAnswerSheets[${index}] is null`);
    }
    (this.quizAnswerSheets[index] as any)[name] = value;
  }

  clear() {
    this.quizAnswerSheets = [];
  }

  async findQuizAnswerSheets(groupId: string, offset: Offset): Promise<QuizAnswerSheet[]> {
    const quizAnswerSheetsDynamicQuery = QuizAnswerSheetsDynamicQuery.oneParam<QuizAnswerSheet[]>(
      QueryParam.endParam('groupId', Operator.Equal, groupId)
    );

    quizAnswerSheetsDynamicQuery.offset = offset;

    const quizAnswerSheets = await this.quizAnswerSheetQueryApi.executeQuizAnswerSheetsDynamicQuery(quizAnswerSheetsDynamicQuery);

    runInAction(() => {
      this.quizAnswerSheets = quizAnswerSheets;
    });

    return quizAnswerSheets;
  }

}

QuizAnswerSheetsStateKeeper.instance = new QuizAnswerSheetsStateKeeper();

export default QuizAnswerSheetsStateKeeper;
