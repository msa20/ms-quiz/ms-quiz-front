import { makeAutoObservable, runInAction } from 'mobx';
import { CommandResponse, NameValueList, QueryParam } from '~/comp/shared';

import {
  QuizAnswer,
  QuizAnswerCdo,
  QuizAnswerDynamicQuery,
  QuizAnswerQueryApiStub,
  QuizSheetApiStub,
} from '~/comp/api/quiz';
import { Operator } from '~/comp';


class QuizAnswerStateKeeper {
  static readonly instanceName: string = 'quizAnswerStateKeeper';
  static instance: QuizAnswerStateKeeper;

  private readonly quizAnswerApi: QuizSheetApiStub;
  private readonly quizAnswerQueryApi: QuizAnswerQueryApiStub;

  quizAnswer: QuizAnswer | null = null;

  constructor(
    quizAnswerApi: QuizSheetApiStub = QuizSheetApiStub.instance,
    quizAnswerQueryApi: QuizAnswerQueryApiStub = QuizAnswerQueryApiStub.instance
  ) {
    this.quizAnswerApi = quizAnswerApi;
    this.quizAnswerQueryApi = quizAnswerQueryApi;
    makeAutoObservable(this);
  }

  init(quizId: string) {
    this.quizAnswer = QuizAnswer.new(quizId);
  }

  setQuizAnswerProp(name: keyof QuizAnswer, value: any) {
    if (!this.quizAnswer) {
      throw new Error('QuizAnswerStateKeeper.setQuizAnswerProp -> quizAnswer is null');
    }
    (this.quizAnswer as any)[name] = value;
  }

  clear() {
    this.quizAnswer = null;
  }

  async register(quizAnswerCdo: QuizAnswerCdo): Promise<CommandResponse> {
    return this.quizAnswerApi.registerQuizAnswer(quizAnswerCdo);
  }

  async modify(quizAnswerId: string, nameValues: NameValueList): Promise<CommandResponse> {
    return this.quizAnswerApi.modifyQuizAnswer(quizAnswerId, nameValues);
  }

  async remove(quizAnswerId: string): Promise<CommandResponse> {
    return this.quizAnswerApi.removeQuizAnswer(quizAnswerId);
  }

  async findQuizAnswerByQuizId(quizId: string, writerId: string): Promise<QuizAnswer | null> {
    //
    const query = QuizAnswerDynamicQuery.multiParams<QuizAnswer>(
      QueryParam.andParam('quizId', Operator.Equal, quizId),
      QueryParam.endParam('writerId', Operator.Equal, writerId),
    );

    const quizAnswer = await this.quizAnswerQueryApi.executeQuizAnswerDynamicQuery(query)
      .catch(() => null);

    runInAction(() => {
      this.quizAnswer = quizAnswer;
    });

    return quizAnswer;
  }

}

QuizAnswerStateKeeper.instance = new QuizAnswerStateKeeper();

export default QuizAnswerStateKeeper;
