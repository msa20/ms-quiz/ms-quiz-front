import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { QuizAnswerSheet } from '~/comp/api';
import { QuizAnswerSheetsStateKeeper } from '~/comp/state';
import QuizAnswerSheetListView from './view/QuizAnswerSheetListView';
import { IdName, Offset, ReactComponent } from '~/comp/shared';


interface Props {
  groupId: string;
  quizSheetId: string;
  writer: IdName;
  limit?: number;
  onClick?: (event: React.MouseEvent, quizAnswerSheet: QuizAnswerSheet) => void;
}

interface State {
}

interface InjectedProps {
  quizAnswerSheetsStateKeeper: QuizAnswerSheetsStateKeeper;
}

@inject(QuizAnswerSheetsStateKeeper.instanceName)
@autobind
@observer
class QuizAnswerSheetListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    limit: 15,
    onClick: () => {},
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { groupId: prevGroupId } = prevProps;
    const { groupId } = this.props;

    if (prevGroupId !== groupId) {
      this.init();
    }
  }

  async init() {
    //
    const { groupId } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizAnswerSheetsStateKeeper } = this.injected;

    const targetOffset = Offset.newDescending(0, limit, 'groupId');

    await quizAnswerSheetsStateKeeper.findQuizAnswerSheets(groupId, targetOffset);
  }

  render() {
    //
    const { writer, onClick } = this.propsWithDefault;
    const { quizAnswerSheets } = this.injected.quizAnswerSheetsStateKeeper;

    return (
      <QuizAnswerSheetListView
        writer={writer}
        quizAnswerSheets={quizAnswerSheets}
        onClick={onClick}
      />
    );
  }
}

export default QuizAnswerSheetListContainer;
