import React from 'react';
import { inject, observer } from 'mobx-react';
import { QuizSheet } from '~/comp/api/quiz';
import { QuizSheetStateKeeper } from '~/comp/state';
import { autobind, IdName, ReactComponent } from '~/comp/shared';
import QuizSheetFormView from './view/QuizSheetFormView';


interface Props {
  groupId: string;
  writer: IdName;
  onSuccess?: () => void;
  onFail?: () => void;
}

interface State {
}

interface InjectedProps {
  quizSheetStateKeeper: QuizSheetStateKeeper;
}

@inject(QuizSheetStateKeeper.instanceName)
@autobind
@observer
class QuizSheetFormContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    //FIXME
    // onSuccess: () => dialogUtil.alert({ message: '등록이 완료되었습니다.' }),
    onSuccess: () => {},
    onFail: () => {},
  };

  componentDidMount() {
    //
    this.init();
  }

  init() {
    //
    const { groupId, writer } = this.props;
    const { quizSheetStateKeeper } = this.injected;

    quizSheetStateKeeper.initSheet(groupId, writer);
  }


  onChange(event: React.ChangeEvent<HTMLInputElement>) {
    //
    const { quizSheetStateKeeper } = this.injected;

    const name = event.target.name as keyof QuizSheet;
    const value = event.target.value;

    quizSheetStateKeeper.setQuizSheetProp(name, value);
  }

  onChangeCategory(event: React.ChangeEvent<{ name?: string; value: unknown }>) {
    //
    const { quizSheetStateKeeper } = this.injected;

    const name = event.target.name as keyof QuizSheet;
    const value = event.target.value;

    quizSheetStateKeeper.setQuizSheetProp(name, value);
  }

  async onSubmit(event: React.MouseEvent) {
    //
    const { onSuccess, onFail } = this.propsWithDefault;
    const { quizSheetStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      throw new Error('QuizSheet should not be null in QuizSheet Component');
    }

    const commandResponse = await quizSheetStateKeeper.save(quizSheet);

    if (commandResponse.entityIds.length > 0) {
      onSuccess();
    }
    else {
      onFail();
    }
  }

  render() {
    //
    const { quizSheetStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      return null;
    }

    return (
      <QuizSheetFormView
        quizSheet={quizSheet}
        onChange={this.onChange}
        onSubmit={this.onSubmit}
        onChangeCategory={this.onChangeCategory}
      />
    );
  }
}

export default QuizSheetFormContainer;
