import React from 'react';
import { inject, observer } from 'mobx-react';
import { QuizSheet } from '~/comp/api';
import { QuizSheetsStateKeeper, QuizSheetStateKeeper } from '~/comp/state';
import QuizSheetListView from './view/QuizSheetListView';
import { Offset, ReactComponent } from '~/comp/shared';



interface Props {
  groupId: string;
  limit?: number;
  onClick?: (event: React.MouseEvent, quizSheet: QuizSheet) => void;
  additionalAction?: React.ReactNode;
  initTrigger?: boolean;
}

interface State {
}

interface InjectedProps {
  quizSheetsStateKeeper: QuizSheetsStateKeeper;
  quizSheetStateKeeper: QuizSheetStateKeeper;
}

@inject(
  QuizSheetsStateKeeper.instanceName,
  QuizSheetStateKeeper.instanceName
)
@observer
class QuizSheetListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    limit: 15,
    onClick: () => {},
    additionalAction: null,
    initTrigger: false,
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { groupId: prevGroupId, initTrigger: prevInitTrigger } = prevProps;
    const { groupId, initTrigger } = this.props;

    if (prevGroupId !== groupId || prevInitTrigger !== initTrigger) {
      this.init();
    }
  }

  async init() {
    //
    const { groupId } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizSheetsStateKeeper } = this.injected;

    const targetOffset = Offset.newDescending(0, limit, 'title');

    await quizSheetsStateKeeper.findQuizSheets(groupId, targetOffset);
  }

  render() {
    //
    const { onClick, additionalAction } = this.propsWithDefault;
    const { quizSheetsStateKeeper } = this.injected;
    const { quizSheets } = quizSheetsStateKeeper;

    console.log('quizSheets--->' + quizSheets.length);


    return (
      <QuizSheetListView
        quizSheets={quizSheets}
        onClick={onClick}
        additionalAction={additionalAction}
      />
    );
  }
}

export default QuizSheetListContainer;
