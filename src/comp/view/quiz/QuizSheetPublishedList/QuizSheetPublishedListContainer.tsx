import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { QuizSheetsStateKeeper, QuizSheetStateKeeper } from '~/comp/state';
import QuizSheetPublishedListView from './view/QuizSheetPublishedListView';
import { Offset, ReactComponent } from '~/comp/shared';


interface Props {
  groupId: string;
  limit?: number;
  onClickButton?: (event: React.MouseEvent, quizSheetId: string) => void;
}

interface State {
  selectedQuizSheetId: string;
}

interface InjectedProps {
  quizSheetStateKeeper: QuizSheetStateKeeper;
  quizSheetsStateKeeper: QuizSheetsStateKeeper;
}

@inject(
  QuizSheetStateKeeper.instanceName,
  QuizSheetsStateKeeper.instanceName
)
@autobind
@observer
class QuizSheetPublishedListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    limit: 15,
    onClickButton: () => {},
  };

  state = { selectedQuizSheetId: '' };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { groupId: prevGroupId } = prevProps;
    const { groupId } = this.props;

    if (prevGroupId !== groupId) {
      this.init();
    }
  }

  async init() {
    //
    const { groupId } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizSheetsStateKeeper } = this.injected;

    const targetOffset = Offset.newDescending(0, limit, 'title');

    await quizSheetsStateKeeper.findQuizSheets(groupId, targetOffset);
  }


  onClickQuizSheet(event: React.ChangeEvent<{ name?: string; value: unknown }>) {
    //
    this.setState({ selectedQuizSheetId: event.target.value as string });
  }

  onClick(event: React.MouseEvent) {
    //
    const { onClickButton } = this.propsWithDefault;
    const { selectedQuizSheetId } = this.state;

    onClickButton(event, selectedQuizSheetId);
  }

  render() {
    //
    const { quizSheetsStateKeeper } = this.injected;
    const { quizSheets } = quizSheetsStateKeeper;

    return (
      <QuizSheetPublishedListView
        quizSheets={quizSheets}
        onClick={this.onClick}
        onClickQuizSheet={this.onClickQuizSheet}
      />
    );
  }
}

export default QuizSheetPublishedListContainer;
