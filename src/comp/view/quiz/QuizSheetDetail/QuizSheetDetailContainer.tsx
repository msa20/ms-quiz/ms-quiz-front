import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { QuizSheet, QuizSheetState } from '~/comp/api';
import { QuizSheetStateKeeper, QuizzesStateKeeper } from '~/comp/state';
import { IdName, ReactComponent } from '~/comp/shared';
import QuizSheetDetailView from './view/QuizSheetDetailView';
import { QuizzesDetail } from '~/comp/view';
import { NWithStyles, nWithStyles } from '~/comp/view/shared/nStyle';



interface Props extends NWithStyles {
  quizSheetId: string;
  writer: IdName;
  onSuccessFinish?: (quizSheet: QuizSheet) => void;
  onFailFinish?: (quizSheet: QuizSheet) => void;
  onSuccessModify?: (quizSheet: QuizSheet) => void;
  onFailModify?: (quizSheet: QuizSheet) => void;
}

interface State {
}

interface InjectedProps {
  quizSheetStateKeeper: QuizSheetStateKeeper;
  quizzesStateKeeper: QuizzesStateKeeper;
}

@inject(QuizSheetStateKeeper.instanceName, QuizzesStateKeeper.instanceName)
@autobind
@observer
class QuizSheetDetailContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onSuccessFinish: (quizSheet: QuizSheet) => {},
    onFailFinish: (quizSheet: QuizSheet) => {},
    onSuccessModify: (quizSheet: QuizSheet) => {},
    onFailModify: (quizSheet: QuizSheet) => {},
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { quizSheetId: prevQuizSheetId } = prevProps;
    const { quizSheetId } = this.props;

    if (prevQuizSheetId !== quizSheetId) {
      this.init();
    }
  }

  async init() {
    //
    const { quizSheetId } = this.props;
    const { quizSheetStateKeeper } = this.injected;

    await quizSheetStateKeeper.findQuizSheetById(quizSheetId);
  }

  onChangeQuizSheet(event: React.ChangeEvent<HTMLInputElement>) {
    //
    const { quizSheetStateKeeper } = this.injected;

    const name = event.target.name as keyof QuizSheet;
    const value = event.target.value;

    quizSheetStateKeeper.setQuizSheetProp(name, value);
  }

  onChangeCategory(event: React.ChangeEvent<{ name?: string; value: unknown }>) {
    //
    const { quizSheetStateKeeper } = this.injected;

    const name = event.target.name as keyof QuizSheet;
    const value = event.target.value;

    quizSheetStateKeeper.setQuizSheetProp(name, value);
  }

  async onClickModify(event: React.MouseEvent) {
    //
    const { onSuccessModify, onFailModify } = this.propsWithDefault;
    const { quizSheetStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      throw new Error('QuizSheet should not be null in QuizSheet Component');
    }

    const commandResponse = await quizSheetStateKeeper.modify(quizSheet.id, QuizSheet.asNameValues(quizSheet));

    quizSheetStateKeeper.setQuizSheetProp('editing', false);

    if (commandResponse.entityIds.length > 0) {
      onSuccessModify(quizSheet);
    }
    else {
      onFailModify(quizSheet);
    }


  }

  async onClickFinish(event: React.MouseEvent) {
    const { onSuccessFinish, onFailFinish } = this.propsWithDefault;
    const { quizSheetStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      throw new Error('QuizSheet should not be null in QuizSheet Component');
    }

    const isValid = await this.isValidFinishing();

    if (!isValid) {
      return;
    }

    quizSheetStateKeeper.setQuizSheetProp('quizSheetState', QuizSheetState.Published);
    const commandResponse = await quizSheetStateKeeper.modify(quizSheet.id, QuizSheet.asNameValues(quizSheet));

    if (commandResponse.entityIds.length > 0) {
      onSuccessFinish(quizSheet);
    }
    else {
      onFailFinish(quizSheet);
    }
  }

  async isValidFinishing() {
    //
    const { quizSheetStateKeeper, quizzesStateKeeper } = this.injected;
    const { quizzes } = quizzesStateKeeper;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      throw new Error('QuizSheet should not be null in QuizSheet Component');
    }

    if (quizzes.length === 0) {
      await alert('문항이 등록되지 않았습니다.');
      return false;
    }

    const quiz = quizzes.find(quiz => quiz.editing === true);

    if (quiz) {
      await alert('수정 중이 문항이 있습니다.');
      return false;
    }

    return true;
  }

  onClickEdit() {
    //
    const { quizSheetStateKeeper } = this.injected;

    quizSheetStateKeeper.setQuizSheetProp('editing', true);
  }


  render() {
    //
    const { writer } = this.props;
    const { quizSheetStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;

    if (!quizSheet) {
      return null;
    }

    return (
      <>
        <QuizSheetDetailView
          quizSheet={quizSheet}
          onChangeQuizSheet={this.onChangeQuizSheet}
          onChangeCategory={this.onChangeCategory}
          onClickModify={this.onClickModify}
          onClickEdit={this.onClickEdit}
          onClickFinish={this.onClickFinish}
        />
        <QuizzesDetail quizSheetId={quizSheet.id} groupId={quizSheet.groupId} writer={writer} />
      </>
    );
  }
}

export default nWithStyles(QuizSheetDetailContainer);
