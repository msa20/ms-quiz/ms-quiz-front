import React from 'react';
import { observer } from 'mobx-react';

import { QuizSheet } from '~/comp/api';
import { QuizSheetState, ReactComponent } from '~/comp';
import EditableView from '~/comp/view/quiz/QuizSheetDetail/view/EditableView';
import ReadonlyView from '~/comp/view/quiz/QuizSheetDetail/view/ReadonlyView';


interface Props {
  quizSheet: QuizSheet;
  onChangeQuizSheet: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChangeCategory: (event: React.ChangeEvent<{ name?: string; value: unknown }>) => void;
  onClickModify: (event: React.MouseEvent) => void;
  onClickEdit: (event: React.MouseEvent) => void;
  onClickFinish: (event: React.MouseEvent) => void;
}

@observer
class QuizSheetDetailView extends ReactComponent<Props> {
  //
  isEditable() {
    //
    const { quizSheet } = this.props;

    return quizSheet.quizSheetState === QuizSheetState.Working && quizSheet.editing;
  }

  render() {
    const { quizSheet, onChangeQuizSheet, onChangeCategory, onClickModify, onClickEdit, onClickFinish } = this.props;


    console.log('isEditable', this.isEditable());
    return this.isEditable() ? (
      <EditableView quizSheet={quizSheet} onChangeQuizSheet={onChangeQuizSheet} onChangeCategory={onChangeCategory} onClickModify={onClickModify} />
    ) : (
      <ReadonlyView quizSheet={quizSheet} onClickEdit={onClickEdit} onClickFinish={onClickFinish} />
    );
  }
}


export default QuizSheetDetailView;
