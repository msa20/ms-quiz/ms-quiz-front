import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import {
  QuizAnswerSheetStateKeeper,
  QuizAnswersStateKeeper,
  QuizAnswerStateKeeper,
  QuizSheetStateKeeper,
  QuizStateKeeper,
  QuizzesStateKeeper,
} from '~/comp/state';
import { IdName, Offset, ReactComponent } from '~/comp/shared';
import SolvingQuizzesFormView from './view/SolvingQuizzesFormView';
import { QuizAnswerSheet } from '~/comp/api';


interface Props {
  groupId: string;
  writer: IdName;
  quizSheetId: string;
  limit?: number;
  onSuccess?: (quizAnswerSheet: QuizAnswerSheet) => void;
  onFail?: (quizAnswerSheet: QuizAnswerSheet) => void;
  handleUnQualification?: () => void;
}

interface State {
}

interface InjectedProps {
  //
  quizStateKeeper: QuizStateKeeper;
  quizzesStateKeeper: QuizzesStateKeeper;
  quizSheetStateKeeper: QuizSheetStateKeeper;
  quizAnswerStateKeeper: QuizAnswerStateKeeper;
  quizAnswersStateKeeper: QuizAnswersStateKeeper;
  quizAnswerSheetStateKeeper: QuizAnswerSheetStateKeeper;
}

@inject(
  QuizStateKeeper.instanceName,
  QuizzesStateKeeper.instanceName,
  QuizSheetStateKeeper.instanceName,
  QuizAnswerStateKeeper.instanceName,
  QuizAnswersStateKeeper.instanceName,
  QuizAnswerSheetStateKeeper.instanceName,
)
@autobind
@observer
class SolvingQuizzesFormContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    limit: 20,
    onSuccess: () => {},
    onFail: () => {},
    handleUnQualification: () => {},
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { quizSheetId: prevQuizSheetId } = prevProps;
    const { quizSheetId } = this.props;

    if (prevQuizSheetId !== quizSheetId) {
      this.init();
    }
  }

  async init() {
    //
    const { quizSheetId, writer } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizAnswerSheetStateKeeper, quizzesStateKeeper, quizSheetStateKeeper, quizAnswersStateKeeper } = this.injected;


    const isValid = await this.checkQuizAnswerSheet();

    if (!isValid) {
      return;
    }

    await quizSheetStateKeeper.takeQuiz(quizSheetId, writer);

    const quizzesOffset = Offset.newAscending(0, limit, 'no');
    const quizAnswersOffset = Offset.newAscending(0, limit, 'quizNo');

    await quizzesStateKeeper.findQuizzesByQuizSheetId(quizSheetId, quizzesOffset); // 이에 맞는 Quiz들 찾아옴
    const quizAnswerSheet = await quizAnswerSheetStateKeeper.findQuizAnswerSheetByQuizSheetId(quizSheetId, writer.id); // 해당 QuizAnswerSheet 찾아옴


    if (!quizAnswerSheet) {
      throw new Error('SolvingQuizzesForm -> There is no quizAnswerSheet with quizSheetId: ' + quizSheetId + 'and writerId: ' + writer.id);
    }
    await quizAnswersStateKeeper.findQuizAnswersByQuizAnswerSheetId(quizAnswerSheet!.id, quizAnswersOffset);
  }

  async checkQuizAnswerSheet() {
    //
    const { quizSheetId, writer } = this.props;
    const { handleUnQualification } = this.propsWithDefault;
    const { quizAnswerSheetStateKeeper } = this.injected;

    const quizAnswerSheet = await quizAnswerSheetStateKeeper.findQuizAnswerSheetByQuizSheetId(quizSheetId, writer.id);

    if (quizAnswerSheet) {
      handleUnQualification();
      return false;
    }

    return true;
  }

  async onClick(event: React.ChangeEvent, index: number, quizItemIndex: string) {
    //
    const { quizzesStateKeeper } = this.injected;

    quizzesStateKeeper.setQuizProp(index, 'checkedAnswer', quizItemIndex);
  }

  async onSubmit(event: React.MouseEvent) {
    //
    const { onSuccess, onFail } = this.propsWithDefault;
    const { quizAnswersStateKeeper, quizAnswerSheetStateKeeper } = this.injected;
    const { quizAnswerSheet } = quizAnswerSheetStateKeeper;

    this.writeAnswers();

    if (!quizAnswerSheet) {
      throw Error('SolvingQuizzesForm -> There is no quizAnswerSheet');
    }

    const commandResponse = await quizAnswersStateKeeper.submitQuiz(quizAnswerSheet.id);

    if (commandResponse.entityIds.length) {
      const finishedQuizAnswerSheet = await quizAnswerSheetStateKeeper.findQuizAnswerSheetById(quizAnswerSheet.id);

      onSuccess(finishedQuizAnswerSheet);
    }
    else {
      onFail(quizAnswerSheet);
    }
  }

  writeAnswers() {
    //
    const { quizzesStateKeeper, quizAnswersStateKeeper } = this.injected;
    const { quizzes } = quizzesStateKeeper;

    quizzes.map((quiz, index) => quizAnswersStateKeeper.setQuizAnswerProp(index, 'itemSeq', quiz.checkedAnswer));
  }


  render() {
    //
    const { quizzesStateKeeper, quizAnswerSheetStateKeeper } = this.injected;
    const { quizzes } = quizzesStateKeeper;
    const { quizAnswerSheet } = quizAnswerSheetStateKeeper;

    if (!quizzes) {
      return null;
    }

    if (!quizAnswerSheet) {
      return null;
    }

    return (
      <SolvingQuizzesFormView
        quizzes={quizzes}
        quizAnswerSheet={quizAnswerSheet}
        onClick={this.onClick}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default SolvingQuizzesFormContainer;
