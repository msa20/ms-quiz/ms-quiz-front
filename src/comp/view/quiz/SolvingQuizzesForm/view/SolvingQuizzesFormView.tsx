import React from 'react';
import { observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { Box, Button, FormControlLabel, Grid, Radio, RadioGroup, Typography } from '@material-ui/core';
import { Quiz, QuizAnswerSheet } from '~/comp/api/quiz';
import { ReactComponent } from '~/comp/shared';


interface Props {
  quizzes: Quiz[];
  quizAnswerSheet: QuizAnswerSheet;
  onClick: (event: React.ChangeEvent, index: number, checkedAnswer: string) => void;
  onSubmit: (event: React.MouseEvent) => void;
}

@autobind
@observer
class SolvingQuizzesFormView extends ReactComponent<Props> {
  //
  render() {
    const { quizzes, quizAnswerSheet, onClick, onSubmit } = this.props;

    return (
      <>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Typography color="textSecondary"> {quizAnswerSheet.writer?.name}</Typography>
          </Box>
          <Box mr={1}>
            <Typography color="textSecondary"> {quizAnswerSheet.subject}</Typography>
          </Box>
          <Box mr={1}>
            <Typography color="textSecondary" gutterBottom>총 {quizzes.length}개</Typography>
          </Box>
        </Box>

        {quizzes.length ? quizzes.map((quiz, index) => (
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <Typography>문제 번호 : {quiz.no}</Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography>QuizCategory : {quiz.quizCategory}</Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography>QuizDifficultyLevel : {quiz.quizDifficultyLevel}</Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography>질문 : {quiz.text}</Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography>추가사항 : {quiz.subText}</Typography>
            </Grid>
            <RadioGroup
              aria-label="itemSeq"
              value={quiz.checkedAnswer}
              onChange={(event, value) => onClick(event, index, value)}
            >
              {quiz.quizItems.length ? quiz.quizItems.map((quizItem, quizItemIndex) => {
                console.log('quizItem---', quizItem);
                console.log('quizItem---', quizItem);
                return (
                  <Grid container>
                    <Grid item xs={3} justifyContent="space-evenly">
                      <FormControlLabel
                        name="itemSeq"
                        value={String(quizItem.sequence)}
                        control={<Radio color="primary" />}
                        label={String(quizItem.sequence)}
                      />
                      <Typography>{quizItem.itemText}</Typography>
                    </Grid>
                  </Grid>
                );
              }) : (
                <Grid item xs={12}>
                  <Typography>문제에 보기가 없습니다.</Typography>
                </Grid>
              )}
            </RadioGroup>
          </Grid>
        )) : (
          <Box display="flex" mt={2} justifyContent="flex-end">
            <Grid>
              <Typography>시험지에 문제가 없습니다.</Typography>
            </Grid>
          </Box>
        )}
        <Box display="flex" justifyContent="center" mt={2} mb={1}>
          <Box mr={1}>
            <Button
              color="primary"
              variant="outlined"
              onClick={onSubmit}
            >
              제출
            </Button>
          </Box>
        </Box>
      </>
    );
  }
}

export default SolvingQuizzesFormView;
