
enum QuizRequiredKey {
  no = 'no',
  quizCategory = 'quizCategory',
  quizDifficultyLevel = 'quizDifficultyLevel',
  text = 'text',
  correctSeq = 'correctSeq',
  quizItems = 'quizItems',
}

export default QuizRequiredKey;
