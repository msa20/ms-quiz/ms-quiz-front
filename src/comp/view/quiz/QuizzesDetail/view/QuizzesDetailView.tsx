import React from 'react';
import { observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import {
  Box,
  Button,
  FormControl, FormControlLabel,
  Grid,
  InputLabel,
  MenuItem, Radio,
  RadioGroup,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { Add, Delete } from '@material-ui/icons';
import { Quiz, QuizCategory, QuizDifficultyLevel } from '~/comp/api';
import { ReactComponent } from '~/comp';


interface Props {
  quizzes: Quiz[];
  onChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) => void;
  onChangeCategory: (event: React.ChangeEvent<{ name?: string; value: unknown }>, index: number) => void;
  onChangeQuizItem: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number, quizItemIndex: number) => void;
  onSubmit: (event: React.MouseEvent, index: number) => void;
  onClickModify: (event: React.MouseEvent, index: number) => void;
  addQuiz: () => void;
  addQuizItem: (index: number) => void;
  onClickRemoveQuizItem: (quizIndex: number, quizItemIndex: number) => void;
  onRemoveQuiz: (event: React.MouseEvent, index: number) => void;
}

@autobind
@observer
class QuizzesDetailView extends ReactComponent<Props> {
  //
  render() {
    const {
      quizzes, onChange, onChangeCategory, onChangeQuizItem, onSubmit,
      onClickModify, addQuiz, addQuizItem, onRemoveQuiz, onClickRemoveQuizItem,
    } = this.props;

    return (
      <>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Typography color="textSecondary" gutterBottom>총 {quizzes.length}개</Typography>
          </Box>
        </Box>
        <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
          <Box mr={1}>
            <Button variant="contained" onClick={() => addQuiz()}>
              문제 추가<Add fontSize="small" />
            </Button>
          </Box>
        </Box>
        {quizzes.map((quiz, index) => ((quiz.quizState === 'Working' && quiz.editing) ? (
          <>
            <Grid container spacing={3}>
              <Grid item xs={3}>
                <TextField
                  required
                  fullWidth
                  label="문제 번호"
                  placeholder={quiz.no}
                  name="no"
                  value={quiz.no}
                  onChange={(event) => onChange(event, index)}
                />
              </Grid>
              <Grid item xs={3}>
                <FormControl fullWidth>
                  <InputLabel id="quizCategory">카테고리 *</InputLabel>
                  <Select
                    required
                    id="quizCategory"
                    labelId="quizCategory"
                    name="quizCategory"
                    value={quiz.quizCategory || ''}
                    onChange={(event) => onChangeCategory(event, index)}
                  >
                    {
                      Object.values(QuizCategory).map(category => (
                        <MenuItem value={category}>{category}</MenuItem>
                      ))
                    }
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={3}>
                <FormControl fullWidth>
                  <InputLabel id="quizDifficultyLevel">난이도 *</InputLabel>
                  <Select
                    required
                    id="quizDifficultyLevel"
                    labelId="quizDifficultyLevel"
                    name="quizDifficultyLevel"
                    value={quiz.quizDifficultyLevel || ''}
                    onChange={(event) => onChangeCategory(event, index)}
                  >
                    {
                      Object.values(QuizDifficultyLevel).map(difficultyLevel => (
                        <MenuItem value={difficultyLevel}>{difficultyLevel}</MenuItem>
                      ))
                    }
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={3}>
                <Typography>정답 : {quiz.correctSeq}</Typography>
              </Grid>
              <Grid item xs={8}>
                <TextField
                  required
                  fullWidth
                  label="문제"
                  placeholder={quiz.text}
                  name="text"
                  value={quiz.text}
                  onChange={(event) => onChange(event, index)}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  fullWidth
                  label="추가사항"
                  placeholder={quiz.subText}
                  name="subText"
                  value={quiz.subText}
                  onChange={(event) => onChange(event, index)}
                />
              </Grid>
              <Box display="flex" justifyContent="flex-end" mt={2} mb={1}>
                <Box mr={1}>
                  <Button variant="contained" onClick={(event) => addQuizItem(index)}>
                    항목 추가<Add fontSize="small" />
                  </Button>
                </Box>
              </Box>

              {quiz.quizItems.length ? (

                <Grid container>
                  {quiz.quizItems.map((quizItem, quizItemIndex) => (
                    <Grid item xs={12}>
                      <RadioGroup aria-label="quiz-item" name="correctSeq" value={quiz.correctSeq} onChange={(event) => onChange(event, index)}>
                        <Grid container>
                          <Grid item xs={2}>
                            <FormControlLabel value={String(quizItem.sequence)} control={<Radio />} label={''} />
                          </Grid>
                          <Grid item xs={9}>
                            <TextField
                              required
                              fullWidth
                              placeholder="문제를 입력하세요"
                              name="itemText"
                              value={quizItem.itemText}
                              onChange={(event) => onChangeQuizItem(event, index, quizItemIndex)}
                            />
                          </Grid>
                          <Grid item xs={1}>
                            <Button variant="contained" onClick={(event) => onClickRemoveQuizItem(index, quizItemIndex)}>
                              <Delete fontSize="small" />
                            </Button>
                          </Grid>
                        </Grid>
                      </RadioGroup>
                    </Grid>
                  ))}
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <Typography>항목을 생성하세요.</Typography>
                </Grid>
              )}
            </Grid>
            <Box display="flex" justifyContent="center" mt={2} mb={1}>
              <Box mr={1}>
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={(event) => onSubmit(event, index)}
                >
                  저장
                </Button>
              </Box>
              <Box mr={1}>
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={(event) => onRemoveQuiz(event, index)}
                >
                  삭제
                </Button>
              </Box>
            </Box>
          </>
        ) : (
          <>
            <Grid container spacing={3}>
              <Grid item xs={3}>
                <Typography>문제 번호 : {quiz.no}</Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography>QuizCategory : {quiz.quizCategory}</Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography>QuizDifficultyLevel : {quiz.quizDifficultyLevel}</Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography>정답 : {quiz.correctSeq}</Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>질문 : {quiz.text}</Typography>
              </Grid>
              <Grid item xs={4}>
                <Typography>추가사항 : {quiz.subText}</Typography>
              </Grid>
              {quiz.quizItems.map((quizItem, quizItemIndex) => (
                <RadioGroup aria-label="quiz-item" name="correctSeq" value={quiz.correctSeq} onChange={(event) => onChange(event, index)}>
                  <FormControlLabel disabled value={String(quizItem.sequence)} control={<Radio />} label={quizItem.itemText} />
                </RadioGroup>
              ))
              }
            </Grid>
            <Box display="flex" justifyContent="center" mt={2} mb={1}>
              <Box mr={1}>
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={(event) => onClickModify(event, index)}
                >
                  수정
                </Button>
              </Box>
              <Box>
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={(event) => onRemoveQuiz(event, index)}
                >
                  삭제
                </Button>
              </Box>
            </Box>
          </>
        )))
        }
      </>
    );
  }
}

export default QuizzesDetailView;
