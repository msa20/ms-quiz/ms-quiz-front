import React from 'react';
import { observer } from 'mobx-react';
import { FormControlLabel, Grid, Radio, RadioGroup, Typography } from '@material-ui/core';
import { IdName, ReactComponent } from '~/comp/shared';
import { Quiz, QuizAnswer, QuizAnswerSheet, QuizGradingResult, QuizSheet } from '~/comp';
import { writer } from '~/storybook/stories/constant';


interface Props {
  writer: IdName;
  quizSheet: QuizSheet;
  quizAnswerSheet: QuizAnswerSheet;
  quizzes: Quiz[];
  quizAnswers: QuizAnswer[];
}

@observer
class QuizAnswerSheetDetailView extends ReactComponent<Props> {
  //
  render() {
    const { quizSheet, quizAnswerSheet, quizzes, quizAnswers } = this.props;

    if (quizAnswers.length === 0) {
      return null;
    }

    return (
      <>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Typography align="center">시험 이름 : {quizSheet.title} (총 {quizzes.length}문제)</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography align="center">응시자 : {writer.name}</Typography>
          </Grid>
        </Grid>

        <Grid container spacing={4}>
          <Grid item xs={11}>
            <Typography align="right">결과 : {quizAnswerSheet.passed ? 'Pass' : 'Fail'}</Typography>
          </Grid>
        </Grid>
        {quizzes.length ? quizzes.map((quiz, quizIndex) => (
          <Grid container spacing={3} key={quiz.id}>

            <Grid item xs={12}>
              <Typography align="left">{quiz.no}. {quiz.text}</Typography>
              {quiz.subText ? (
                <Typography align="left">({quiz.subText})</Typography>
              ) : null
              }
            </Grid>
            {quiz.quizItems.map((quizItem, quizItemIndex) => (
              <RadioGroup aria-label="quiz-item" name="correctSeq" value={quizAnswers[quizIndex].itemSeq}>
                <FormControlLabel
                  disabled
                  value={String(quizItem.sequence)}
                  control={<Radio />}
                  label={quizItem.itemText}
                />
              </RadioGroup>

            ))
            }
            {quizAnswers[quizIndex].quizGradingResult === QuizGradingResult.InCorrect ? (
              <Grid container>
                <Grid item xs={2}>
                  <Typography color="error"> 정답:{quiz.correctSeq}번</Typography>
                </Grid>
              </Grid>
            ) : null
            }


            <br /><br /><br />
          </Grid>
        )) : (
          <Grid container spacing={1}>
            <Grid item xs={4}>
              ??
            </Grid>
          </Grid>

        )}
      </>
    );
  }
}

export default QuizAnswerSheetDetailView;
