import React from 'react';
import { inject, observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import {
  QuizAnswerSheetStateKeeper, QuizAnswersStateKeeper,
  QuizSheetStateKeeper,
  QuizzesStateKeeper,
} from '~/comp/state';
import QuizAnswerSheetDetailView from './view/QuizAnswerSheetDetailView';
import { IdName, Offset, ReactComponent } from '~/comp/shared';


interface Props {
  groupId: string;
  quizAnswerSheetId: string;
  writer: IdName;
  limit?: number;
  onSuccess?: () => void;
}

interface State {
}

interface InjectedProps {
  //
  quizSheetStateKeeper: QuizSheetStateKeeper;
  quizAnswerSheetStateKeeper: QuizAnswerSheetStateKeeper;
  quizzesStateKeeper: QuizzesStateKeeper;

  quizAnswersStateKeeper: QuizAnswersStateKeeper;   // fixme quizAnswer 가져오기 위함
}

@inject(
  QuizSheetStateKeeper.instanceName,
  QuizAnswerSheetStateKeeper.instanceName,
  QuizzesStateKeeper.instanceName,
  QuizAnswersStateKeeper.instanceName
)
@autobind
@observer
class QuizAnswerSheetDetailContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onSuccess: () => {},
    limit: 20,
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { quizAnswerSheetId: prevQuizAnswerSheetId } = prevProps;
    const { quizAnswerSheetId } = this.props;

    if (prevQuizAnswerSheetId !== quizAnswerSheetId) {
      this.init();
    }
  }

  async init() {
    //
    const { quizAnswerSheetId } = this.props;
    const { limit } = this.propsWithDefault;
    const { quizAnswerSheetStateKeeper, quizSheetStateKeeper, quizzesStateKeeper, quizAnswersStateKeeper } = this.injected;

    const quizAnswerSheet = await quizAnswerSheetStateKeeper.findQuizAnswerSheetById(quizAnswerSheetId);

    if (!quizAnswerSheet) {
      return;
    }

    const quizSheet = await quizSheetStateKeeper.findQuizSheetById(quizAnswerSheet.quizSheetId);

    if (!quizSheet) {
      return;
    }

    const quizOffset = Offset.newDescending(0, limit, 'no');
    const quizAnswerOffset = Offset.newDescending(0, limit, 'quizNo');

    await quizzesStateKeeper.findQuizzesByQuizSheetId(quizSheet.id, quizOffset);

    // fixme : quizAnswers 가져오기
    await quizAnswersStateKeeper.findQuizAnswersByQuizAnswerSheetId(quizAnswerSheetId, quizAnswerOffset);
  }

  render() {
    //
    const { writer } = this.props;
    const { quizAnswersStateKeeper, quizSheetStateKeeper, quizAnswerSheetStateKeeper, quizzesStateKeeper } = this.injected;
    const { quizSheet } = quizSheetStateKeeper;
    const { quizAnswerSheet } = quizAnswerSheetStateKeeper;
    const { quizzes } = quizzesStateKeeper;
    const { quizAnswers } = quizAnswersStateKeeper;

    if (!quizSheet) {
      return null;
    }

    if (!quizAnswerSheet) {
      return null;
    }

    return (
      <QuizAnswerSheetDetailView
        writer={writer}
        quizSheet={quizSheet}
        quizAnswerSheet={quizAnswerSheet}
        quizzes={quizzes}
        quizAnswers={quizAnswers}
      />
    );
  }
}

export default QuizAnswerSheetDetailContainer;
