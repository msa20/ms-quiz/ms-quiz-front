import React from 'react';
import { observer } from 'mobx-react';
import { TextField, TextFieldProps } from '@material-ui/core';
import autobind from 'autobind-decorator';
import { ReactComponent } from '~/comp';


type Props = Omit<TextFieldProps, 'value'> & {
  value: string;
}


@autobind
@observer
class ContentView extends ReactComponent<Props> {
  //
  static defaultProps = {
    placeholder: '내용을 입력하세요',
    rows: 1,
    onChange: () => {},
  };

  render() {
    const { value, rows, onChange, ...otherProps } = this.propsWithDefault;

    return (
      <TextField
        {...otherProps}
        fullWidth
        multiline={rows !== 1}
        rows={rows}
        rowsMax={rows}
        value={value || ''}
        onChange={onChange}
      />
    );
  }
}

export default ContentView;
