import React from 'react';
import autobind from 'autobind-decorator';
import { observer } from 'mobx-react';
import { Done } from '@material-ui/icons';
import { Box, Button } from '@material-ui/core';
import { withStyles, WithStyles } from './style';
import { ReactComponent } from '~/comp/shared';


interface Props extends WithStyles {
  onSubmit?: (event: React.MouseEvent) => void;
}

@autobind
@observer
class ActionViews extends ReactComponent<Props> {
  static defaultProps = {
    onSubmit: () => {},
  };

  render() {
    const { onSubmit } = this.propsWithDefault;

    return (
      <Box pt={1} textAlign="center">
        <Box m={'auto'} textAlign="center">
          <Button color="primary" variant="contained" size="small" onClick={onSubmit}><Done /> 등록</Button>
        </Box>
      </Box>
    );
  }
}

export default withStyles(ActionViews);
