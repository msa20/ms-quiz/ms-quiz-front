import React from 'react';
import { observer } from 'mobx-react';
import autobind from 'autobind-decorator';
import { ReactComponent } from '~/comp/shared';


interface Props {
  children: React.ReactNode;
}


@autobind
@observer
class QuizFormBaseView extends ReactComponent<Props> {
  //
  render() {
    //
    return this.props.children;
  }
}

export default QuizFormBaseView;
