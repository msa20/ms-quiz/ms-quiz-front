
import { createStyles, Theme, WithStyles as MuiWithStyles, withStyles as muiWithStyles } from '@material-ui/core/styles';
// import { deepOrange, deepPurple, green, pink } from '@material-ui/core/colors';


const style = (theme: Theme) => createStyles({
});

export default style;
export type NWithStyles = MuiWithStyles<typeof style>;
export const nWithStyles = muiWithStyles(style);
