
import { Component } from 'react';


class ReactComponent<Props = {}, State = {}, InjectedProps = {}> extends Component<Props, State> {
  //
  private isDefaultPropsDefined(): boolean {
    //
    const defaultProps = (this.constructor as any).defaultProps;

    return defaultProps && Object.keys(defaultProps).length > 0;
  }

  get injected(): InjectedProps {
    //
    return this.props as any;
  }

  get propsWithDefault(): Required<Props> {
    //
    if (!this.isDefaultPropsDefined()) {
      throw new Error('ReactComponent -> propsWithDefault, it must define defaultProps');
    }

    return this.props as Required<Props>;
  }
}

export default ReactComponent;
