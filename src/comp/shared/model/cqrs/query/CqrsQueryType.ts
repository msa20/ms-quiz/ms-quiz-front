enum CqrsQueryType {
  //
  BaseQuery = 'BaseQuery',       // Basic CUD command
  UserDefineQuery = 'UserDefineQuery',
  DynamicQuery = 'DynamicQuery'
}

export default CqrsQueryType;
