
import CqrsBaseCommandType from './CqrsBaseCommandType';


class CqrsBaseCommand {
  //
  cqrsBaseCommandType: CqrsBaseCommandType;


  constructor(type: CqrsBaseCommandType) {
    //
    this.cqrsBaseCommandType = type;
  }
}

export default CqrsBaseCommand;
