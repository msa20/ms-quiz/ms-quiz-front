enum CqrsCommandType {
  //
  BaseCommand = 'BaseCommand',       // Basic CUD command
  UserDefineCommand = 'UserDefineCommand'
}

export default CqrsCommandType;
