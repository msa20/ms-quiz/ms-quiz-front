import { makeObservable, observable } from 'mobx';


abstract class DomainEntity {
  //
  id: string = '';
  entityVersion: number = 0;
  registrationTime: number = 0;
  modificationTime: number = 0;

  constructor(domainEntity?: DomainEntity) {
    //
    if (domainEntity) {
      this.id = domainEntity.id;
      this.entityVersion = domainEntity.entityVersion;
      this.registrationTime = domainEntity.registrationTime;
      this.modificationTime = domainEntity.modificationTime;
    }

    makeObservable(this, {
      id: observable,
      entityVersion: observable,
      registrationTime: observable,
      modificationTime: observable,
    });
  }

  setDomainEntity(domainEntity: DomainEntity) {
    //
    this.id = domainEntity.id;
    this.entityVersion = domainEntity.entityVersion;
    this.registrationTime = domainEntity.registrationTime;
    this.modificationTime = domainEntity.modificationTime;
  }
}

export default DomainEntity;
