import axios from 'axios';
import {
  QuizDynamicQuery,
  QuizQuery,
  QuizzesDynamicQuery,
} from '../query';
import { Quiz } from '../../api-model';


class QuizQueryApiStub {
  static instance: QuizQueryApiStub;

  private readonly quizQueryApiUri: string = '/api/quiz/secure/quiz/query';

  async executeQuizQuery(query: QuizQuery): Promise<Quiz> {
    return axios.post(`${this.quizQueryApiUri}/`, query)
      .then((response) => response && response.data && response.data.queryResult && Quiz.fromDomain(response.data.queryResult));
  }

  async executeQuizDynamicQuery(query: QuizDynamicQuery): Promise<Quiz | null> {
    return axios.post(`${this.quizQueryApiUri}/dynamic-single`, query)
      .then((response) => response && response.data && response.data.queryResult && Quiz.fromDomain(response.data.queryResult));
  }

  async executeQuizzesDynamicQuery(query: QuizzesDynamicQuery): Promise<Quiz[]> {
    return axios.post(`${this.quizQueryApiUri}/dynamic-multi`, query)
      .then((response) => response && response.data && Quiz.fromDomains(response.data.queryResult));
  }

}

QuizQueryApiStub.instance = new QuizQueryApiStub();

export default QuizQueryApiStub;
