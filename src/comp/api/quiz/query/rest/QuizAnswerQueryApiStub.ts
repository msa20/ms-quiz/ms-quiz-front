import axios from 'axios';
import { QuizAnswer } from '../../api-model';
import { QuizAnswerQuery, QuizAnswerDynamicQuery, QuizAnswersDynamicQuery } from '../query';


class QuizAnswerQueryApiStub {
  static instance: QuizAnswerQueryApiStub;

  private readonly quizAnswerQueryApiUri: string = '/api/quiz/secure/quiz-answer/query';

  async executeQuizAnswerQuery(query: QuizAnswerQuery): Promise<QuizAnswer> {
    return axios.post(`${this.quizAnswerQueryApiUri}/`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizAnswer.fromDomain(response.data.queryResult));
  }

  async executeQuizAnswerDynamicQuery(query: QuizAnswerDynamicQuery): Promise<QuizAnswer | null> {
    return axios.post(`${this.quizAnswerQueryApiUri}/dynamic-single`, query)
      .then((response) => response && response.data && response.data.queryResult && QuizAnswer.fromDomain(response.data.queryResult));
  }

  async executeQuizAnswersDynamicQuery(query: QuizAnswersDynamicQuery): Promise<QuizAnswer[]> {
    console.log('---------', query);
    return axios.post(`${this.quizAnswerQueryApiUri}/dynamic-multi`, query)
      .then((response) => response && response.data && QuizAnswer.fromDomains(response.data.queryResult));
  }
}

QuizAnswerQueryApiStub.instance = new QuizAnswerQueryApiStub();

export default QuizAnswerQueryApiStub;
