import { CqrsBaseQuery } from '~/comp/shared';
import { QuizAnswerSheet } from '../../api-model';


class QuizAnswerSheetQuery extends CqrsBaseQuery<QuizAnswerSheet> {
  quizAnswerSheetId: string;

  constructor(quizAnswerSheetId: string) {
    super();
    this.quizAnswerSheetId = quizAnswerSheetId;
  }

  static by(quizAnswerSheetId: string) {
    const query = new QuizAnswerSheetQuery(quizAnswerSheetId);
    return query;
  }
}

export default QuizAnswerSheetQuery;
