import { CqrsBaseQuery } from '~/comp/shared';
import { QuizAnswer } from '../../api-model';


class QuizAnswerQuery extends CqrsBaseQuery<QuizAnswer> {
  quizAnswerId: string;

  constructor(quizAnswerId: string) {
    super();
    this.quizAnswerId = quizAnswerId;
  }

  static by(quizAnswerId: string) {
    return new QuizAnswerQuery(quizAnswerId);
  }

}

export default QuizAnswerQuery;
