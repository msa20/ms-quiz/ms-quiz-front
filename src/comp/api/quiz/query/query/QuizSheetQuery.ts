import { CqrsBaseQuery } from '~/comp/shared';
import { QuizSheet } from '../../api-model';


class QuizSheetQuery extends CqrsBaseQuery<QuizSheet> {
  quizSheetId: string;

  constructor(quizSheetId: string) {
    super();
    this.quizSheetId = quizSheetId;
  }

  static by(quizSheetId: string) {
    const query = new QuizSheetQuery(quizSheetId);
    return query;
  }

}

export default QuizSheetQuery;
