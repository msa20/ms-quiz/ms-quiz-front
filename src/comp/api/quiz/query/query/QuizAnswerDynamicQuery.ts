import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizAnswer } from '../../api-model';


class QuizAnswerDynamicQuery extends CqrsDynamicQuery<QuizAnswer> {
}

export default QuizAnswerDynamicQuery;
