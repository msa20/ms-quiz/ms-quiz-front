import { CqrsDynamicQuery } from '~/comp/shared';
import { QuizAnswer } from '../../api-model';


class QuizAnswerSheetsDynamicQuery extends CqrsDynamicQuery<QuizAnswer[]> {
}

export default QuizAnswerSheetsDynamicQuery;
