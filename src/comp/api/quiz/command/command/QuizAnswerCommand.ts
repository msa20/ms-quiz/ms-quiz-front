import { CqrsBaseCommand, CqrsBaseCommandType, NameValueList } from '~/comp/shared';
import { QuizAnswerCdo } from '../../api-model';


class QuizAnswerCommand extends CqrsBaseCommand {
  quizAnswerCdo: QuizAnswerCdo | null = null;
  quizAnswerCdos: QuizAnswerCdo[] = [];
  quizAnswerId: string | null = null;
  nameValues: NameValueList | null = null;

  static newRegisterQuizAnswerCommand(quizAnswerCdo: QuizAnswerCdo): QuizAnswerCommand {
    const command = new QuizAnswerCommand(CqrsBaseCommandType.Register);

    command.quizAnswerCdo = quizAnswerCdo;
    return command;
  }

  static newRegisterQuizAnswerCommands(quizAnswerCdos: QuizAnswerCdo[]): QuizAnswerCommand {
    const command = new QuizAnswerCommand(CqrsBaseCommandType.Register);

    command.quizAnswerCdos = quizAnswerCdos;
    return command;
  }

  static newModifyQuizAnswerCommand(quizAnswerId: string, nameValues: NameValueList): QuizAnswerCommand {
    const command = new QuizAnswerCommand(CqrsBaseCommandType.Modify);

    command.quizAnswerId = quizAnswerId;
    command.nameValues = nameValues;
    return command;
  }

  static newRemoveQuizAnswerCommand(quizAnswerId: string): QuizAnswerCommand {
    const command = new QuizAnswerCommand(CqrsBaseCommandType.Remove);

    command.quizAnswerId = quizAnswerId;
    return command;
  }
}

export default QuizAnswerCommand;
