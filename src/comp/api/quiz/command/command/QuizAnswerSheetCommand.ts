import { CqrsBaseCommand, CqrsBaseCommandType, NameValueList } from '~/comp/shared';
import { QuizAnswerSheetCdo } from '../../api-model';


class QuizAnswerSheetCommand extends CqrsBaseCommand {
  quizAnswerSheetCdo: QuizAnswerSheetCdo | null = null;
  quizAnswerSheetCdos: QuizAnswerSheetCdo[] = [];
  quizAnswerSheetId: string | null = null;
  nameValues: NameValueList | null = null;

  static newRegisterQuizAnswerSheetCommand(quizAnswerSheetCdo: QuizAnswerSheetCdo): QuizAnswerSheetCommand {
    const command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Register);

    command.quizAnswerSheetCdo = quizAnswerSheetCdo;
    return command;
  }

  static newRegisterQuizAnswerSheetCommands(quizAnswerSheetCdos: QuizAnswerSheetCdo[]): QuizAnswerSheetCommand {
    const command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Register);

    command.quizAnswerSheetCdos = quizAnswerSheetCdos;
    return command;
  }

  static newModifyQuizAnswerSheetCommand(quizAnswerSheetId: string, nameValues: NameValueList): QuizAnswerSheetCommand {
    const command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Modify);

    command.quizAnswerSheetId = quizAnswerSheetId;
    command.nameValues = nameValues;
    return command;
  }

  static newRemoveQuizAnswerSheetCommand(quizAnswerSheetId: string): QuizAnswerSheetCommand {
    const command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Remove);

    command.quizAnswerSheetId = quizAnswerSheetId;
    return command;
  }
}

export default QuizAnswerSheetCommand;
