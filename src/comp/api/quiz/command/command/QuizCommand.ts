import { CqrsBaseCommand, CqrsBaseCommandType, NameValueList } from '~/comp/shared';
import { QuizCdo } from '../../api-model';


class QuizCommand extends CqrsBaseCommand {
  quizCdo: QuizCdo | null = null;
  quizCdos: QuizCdo[] = [];
  quizId: string | null = null;
  nameValues: NameValueList | null = null;

  static newRegisterQuizCommand(quizCdo: QuizCdo): QuizCommand {
    const command = new QuizCommand(CqrsBaseCommandType.Register);

    command.quizCdo = quizCdo;
    return command;
  }

  static newRegisterQuizCommands(quizCdos: QuizCdo[]): QuizCommand {
    const command = new QuizCommand(CqrsBaseCommandType.Register);

    command.quizCdos = quizCdos;
    return command;
  }

  static newModifyQuizCommand(quizId: string, nameValues: NameValueList): QuizCommand {
    const command = new QuizCommand(CqrsBaseCommandType.Modify);

    command.quizId = quizId;
    command.nameValues = nameValues;
    return command;
  }

  static newRemoveQuizCommand(quizId: string): QuizCommand {
    const command = new QuizCommand(CqrsBaseCommandType.Remove);

    command.quizId = quizId;
    return command;
  }
}

export default QuizCommand;
