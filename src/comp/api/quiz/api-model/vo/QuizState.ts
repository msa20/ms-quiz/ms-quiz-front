enum QuizState {
  Working = 'Working',
  Published = 'Published'
}

export default QuizState;
