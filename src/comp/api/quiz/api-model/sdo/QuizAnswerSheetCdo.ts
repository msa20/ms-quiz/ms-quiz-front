import { IdName, validationUtils } from '~/comp/shared';
import QuizAnswerSheet from '../QuizAnswerSheet';


class QuizAnswerSheetCdo {
  groupId: string;
  quizSheetId: string;
  writer: IdName;
  subject: string;

  constructor(
    groupId: string,
    quizSheetId: string,
    writer: IdName,
    subject: string,
  ) {
    this.groupId = groupId;
    this.quizSheetId = quizSheetId;
    this.writer = writer;
    this.subject = subject;
  }

  static fromModel(domain: QuizAnswerSheet) {
    const params = validationUtils.checkNullableParams<QuizAnswerSheet, keyof QuizAnswerSheet>(
      domain,
      [
        'writer',
      ]
    );

    return new QuizAnswerSheetCdo(
      domain.groupId,
      domain.quizSheetId,
      params.writer,
      domain.subject,
    );
  }
}

export default QuizAnswerSheetCdo;
