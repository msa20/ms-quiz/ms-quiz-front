import { IdName, validationUtils } from '~/comp/shared';
import { QuizCategory, QuizDifficultyLevel, QuizItem, QuizState } from '../vo';
import Quiz from '../Quiz';


class QuizCdo {
  groupId: string;
  quizSheetId: string;

  writer: IdName;
  no: string;
  text: string;
  subText: string;
  quizCategory: QuizCategory;

  quizDifficultyLevel: QuizDifficultyLevel;
  quizState: QuizState;
  correctSeq: string;
  quizItems: QuizItem[];

  constructor(
    groupId: string,
    quizSheetId: string,
    writer: IdName,
    no: string,
    text: string,
    subText: string,
    quizCategory: QuizCategory,
    quizDifficultyLevel: QuizDifficultyLevel,
    quizState: QuizState,
    correctSeq: string,
    quizItems: QuizItem[],
  ) {
    this.groupId = groupId;
    this.quizSheetId = quizSheetId;
    this.writer = writer;
    this.no = no;
    this.text = text;
    this.subText = subText;
    this.quizCategory = quizCategory;
    this.quizDifficultyLevel = quizDifficultyLevel;
    this.quizState = quizState;
    this.correctSeq = correctSeq;
    this.quizItems = quizItems;
  }

  //
  static fromModel(domain: Quiz) {
    console.log('cdo domain', domain);
    const params = validationUtils.checkNullableParams<Quiz, keyof Quiz>(
      domain,
      [
        'writer',
        'quizCategory',
        'quizDifficultyLevel',
      ]
    );

    return new QuizCdo(
      domain.groupId,
      domain.quizSheetId,
      params.writer,
      domain.no,
      domain.text,
      domain.subText,
      params.quizCategory,
      params.quizDifficultyLevel,
      domain.quizState,
      domain.correctSeq,
      domain.quizItems,
    );
  }

  static fromModels(domains: Quiz[]) {
    return domains.map(domain => this.fromModel(domain));
  }
}

export default QuizCdo;
