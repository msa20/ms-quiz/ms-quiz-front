import { CqrsUserCommand, IdName } from '~/comp/shared';


class TakeQuizCommand extends CqrsUserCommand {
  quizSheetId: string;
  writer: IdName;

  constructor(quizSheetId: string, writer: IdName) {
    super();
    this.quizSheetId = quizSheetId;
    this.writer = writer;
  }

  static new(quizSheetId: string, writer: IdName) {
    const command = new TakeQuizCommand(
      quizSheetId,
      writer,
    );
    return command;
  }

}

export default TakeQuizCommand;
