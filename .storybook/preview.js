
// import '../public/css/2.3bdd35f2.chunk.css';
// import '../public/css/main.ac1c28d3.chunk.css';

import configurePreview from '@nara.platform/storybook/storyConfig/configurePreview';

import React from 'react';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';
import { store } from '@nextree-ms/quiz';


configure({
  enforceActions: 'observed',
  isolateGlobalState: true,
});

const { parameters, decorators } = configurePreview(defaultConfig => {
  //
  const customizedConfig = defaultConfig;

  const additionalDecorator = Story => {
    console.log('store', store);
    return (
      <Provider
        {...store}
      >
        <Story />
      </Provider>
    )
  }

  customizedConfig.decorators.push(additionalDecorator);
  customizedConfig.parameters.viewMode = 'canvas';

  return customizedConfig;
});

export {
  parameters,
  decorators,
};
