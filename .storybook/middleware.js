
const configureMiddleware = require('@nara.platform/storybook/storyConfig/configureMiddleware');
const proxy = require('http-proxy-middleware');


module.exports = configureMiddleware(router => {
  //
  // router.baseTarget = 'http://34.84.95.166';

  router.use('/api/quiz', proxy({
    target: 'http://localhost:8080',
    pathRewrite: { '/api/quiz': '/' },
    secure: false,
    crossOrigin: true,
  }));
});
